"""
You are required to write a program to sort the (name, age, height) tuples by ascending order where name is string, age and height are numbers. The tuples are input by console. The sort criteria is:
1: Sort based on name;
2: Then sort based on age;
3: Then sort by score.
The priority is that name > age > score.
"""
from operator import itemgetter

def multiline():
    while True:
        line = input()
        if line:
            l = line.split(",")
            yield tuple(l)
        else:
            break

# sort by name, age and score
input_by_score = sorted(list(multiline()), key=itemgetter(0,1,2))

for t in input_by_score:
    print(*t, sep=",")

