"""
Write a program that accepts sequence of lines as input and prints the lines after making all characters in the sentence capitalized.
Suppose the following input is supplied to the program:
Hello world
Practice makes perfect
Then, the output should be:
HELLO WORLD
PRACTICE MAKES PERFECT
"""

def multiline_input():
    while True:
        try:
            inp = input()
        except:
            return
        if not inp:
            break
        yield inp.upper()

print(*list(multiline_input()), sep="\n")


