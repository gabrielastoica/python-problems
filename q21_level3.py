"""
A robot moves in a plane starting from the original point (0,0). The robot can move toward UP, DOWN, LEFT and RIGHT with a given steps. The trace of robot movement is shown as the following:
UP 5
DOWN 3
LEFT 3
RIGHT 2
¡­
The numbers after the direction are steps. Please write a program to compute the distance from current position after a sequence of movement and original point. If the distance is a float, then just print the nearest integer.
Example:
If the following tuples are given as input to the program:
UP 5
DOWN 3
LEFT 3
RIGHT 2
Then, the output of the program should be:
2
"""

from math import sqrt

def up(y, steps):
    return y + steps

def down(y, steps):
    return y - steps

def left(x, steps):
    return  x - steps

def right(x, steps):
    return x + steps

def distance(x, y):
    return round(sqrt(x**2 + y**2))

def parse_input():
    x = 0
    y = 0
    while True:
        try:
            inp = input().upper().split()
        except:
            return
        if not inp:
            break
        direction = inp[0]
        steps = int(inp[1])
        if direction == 'UP':
            y = up(y, steps)
        elif direction == 'DOWN':
            y = down(y, steps)
        elif direction == 'LEFT':
            x = left(x, steps)
        elif direction == 'RIGHT':
            x = right(x, steps)
    return distance(x,y)

print(parse_input())


