"""
Question:
Define a class with a generator which can iterate the numbers, which are divisible by 7, between a given range 0 and n.
"""

class Div7:
    def __init__(self, n):
        self.n = n

    def div7(self):
        i = 0
        while i < self.n :
            if i%7 == 0:
                yield i
            i += 1

n = Div7(100)
print(list(n.div7()))

